package org.example.model;

/**
 * Class representing film form database
 */
public class Film {
    private int id;
    private String title;
    private String day;
    public boolean m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12,m13,m14,m15,m16,m17,m18,m19,m20;

    /**
     * Get id from object
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * Get title from object
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Get day from object
     * @return day
     */
    public String getDay() {
        return day;
    }

    /**
     * Constructor for object film
     * @param id Film id
     * @param title Film title
     * @param day Day when film start
     * @param m1 seat in cinema
     * @param m2 seat in cinema
     * @param m3 seat in cinema
     * @param m4 seat in cinema
     * @param m5 seat in cinema
     * @param m6 seat in cinema
     * @param m7 seat in cinema
     * @param m8 seat in cinema
     * @param m9 seat in cinema
     * @param m10 seat in cinema
     * @param m11 seat in cinema
     * @param m12 seat in cinema
     * @param m13 seat in cinema
     * @param m14 seat in cinema
     * @param m15 seat in cinema
     * @param m16 seat in cinema
     * @param m17 seat in cinema
     * @param m18 seat in cinema
     * @param m19 seat in cinema
     * @param m20 seat in cinema
     */
    public Film(int id, String title, String day, boolean m1, boolean m2, boolean m3, boolean m4, boolean m5, boolean m6, boolean m7, boolean m8, boolean m9, boolean m10, boolean m11, boolean m12, boolean m13, boolean m14, boolean m15, boolean m16, boolean m17, boolean m18, boolean m19, boolean m20){
        this.id = id;
        this.title = title;
        this.day = day;

        this.m1 =m1;
        this.m2 =m2;
        this.m3 =m3;
        this.m4 =m4;
        this.m5 =m5;
        this.m6 =m6;
        this.m7 =m7;
        this.m8 =m8;
        this.m9 =m9;
        this.m10 =m10;
        this.m11 =m11;
        this.m12 =m12;
        this.m13 =m13;
        this.m14 =m14;
        this.m15 =m15;
        this.m16 =m16;
        this.m17 =m17;
        this.m18 =m18;
        this.m19 =m19;
        this.m20 =m20;

    }

    /**
     * Make from object string type
     * @return id+" "+title
     */
    @Override
    public String toString() {
        return id+" "+title;
    }
}

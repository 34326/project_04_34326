package org.example.database;

import org.example.model.Film;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Class responsible for connecting with database
 */
public class SQLite {

    public static final String DRIVER = "org.sqlite.JDBC";
    public static final String DB_URL = "jdbc:sqlite:cinemadb.db";

    private static Connection conn;
    private Statement stat;

    /**
     * Constructor for creating connection with database
     */
    public SQLite() {
        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException e) {
            System.err.println("Brak sterownika JDBC");
            e.printStackTrace();
        }

        try {
            conn = DriverManager.getConnection(DB_URL);
            stat = conn.createStatement();
        } catch (SQLException e) {
            System.err.println("Problem z otwarciem polaczenia");
            e.printStackTrace();
        }

    }

    /**
     * Creating objects from database records to operate on these items.
     * @param part variable for selecting movie from database
     * @return films
     */
    public List<Film> selectFilms(String part) {
        List<Film> films = new LinkedList<Film>();
        try {
            ResultSet result = stat.executeQuery("SELECT * FROM Films WHERE day like '"+part+"'");
            int id;
            String title, day;
            boolean m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12,m13,m14,m15,m16,m17,m18,m19,m20;
            while(result.next()) {
                id = result.getInt("ID");
                title = result.getString("Title");
                day = result.getString("Day");
                m1=result.getBoolean("M1");
                m2=result.getBoolean("M2");
                m3=result.getBoolean("M3");
                m4=result.getBoolean("M4");
                m5=result.getBoolean("M5");
                m6=result.getBoolean("M6");
                m7=result.getBoolean("M7");
                m8=result.getBoolean("M8");
                m9=result.getBoolean("M9");
                m10=result.getBoolean("M10");
                m11=result.getBoolean("M11");
                m12=result.getBoolean("M12");
                m13=result.getBoolean("M13");
                m14=result.getBoolean("M14");
                m15=result.getBoolean("M15");
                m16=result.getBoolean("M16");
                m17=result.getBoolean("M17");
                m18=result.getBoolean("M18");
                m19=result.getBoolean("M19");
                m20=result.getBoolean("M20");
                films.add(new Film(id,title,day,m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12,m13,m14,m15,m16,m17,m18,m19,m20));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return films;
    }

    /**
     * Add true values on seats in cinema
     * @param m variable which select when we exactly want to take seat
     * @param id variable for checking on which films we operte on films
     * @return true, false
     */
    public boolean insertFilm(Integer m, Integer id){
        try {
            stat.executeUpdate("Update Films SET M"+m+"='1' where id="+id+";");
        } catch (SQLException e) {
            System.err.println("Blad przy wstawianiu miejsca");
            e.printStackTrace();
            return false;
        }
        return true;

    }


    /**
     * Close connection in database
     */
    public void closeConnection() {
        try {
            conn.close();
        } catch (SQLException e) {
            System.err.println("Problem z zamknieciem polaczenia");
            e.printStackTrace();
        }
    }
}

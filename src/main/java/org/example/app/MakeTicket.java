package org.example.app;

import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.xobject.PdfFormXObject;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.barcodes.BarcodeQRCode;
import com.itextpdf.layout.property.TextAlignment;


import java.io.FileNotFoundException;

/**
 * Class responsible for generating a pdf ticket and qr code
 */
public class MakeTicket {
    /**
     * Public method responsible for generating a pdf ticket and qr code
     * @param title title of the movie
     * @param date date of the screening in the cinema
     * @param userName booking person's details
     * @throws FileNotFoundException
     */
    public static void makeTicket(String title, String date, String userName) throws FileNotFoundException {
        PdfWriter pdfWriter = new PdfWriter("Ticket.pdf");
        PdfDocument pdfDocument = new PdfDocument(pdfWriter);
        pdfDocument.addNewPage();

        Paragraph paragraph2 = new Paragraph("Bilet na film:" + " " + title);
        paragraph2.setTextAlignment(TextAlignment.CENTER);
        Paragraph paragraph3 = new Paragraph("Data seansu:" + " " + date);
        paragraph3.setTextAlignment(TextAlignment.CENTER);
        Paragraph paragraph4 = new Paragraph("Osoba rezerwujaca bilet:" + " " + userName);
        paragraph4.setTextAlignment(TextAlignment.CENTER);
        Document document = new Document(pdfDocument);
        BarcodeQRCode qrCode = new BarcodeQRCode(title + " " + date + " " + userName);
        PdfFormXObject barcodeObject = qrCode.createFormXObject(Color.BLACK, pdfDocument);
        Image barcodeImage = new Image(barcodeObject).setWidth(100f).setHeight(100f);
        Paragraph paragraph1 = new Paragraph().add(barcodeImage);
        paragraph1.setTextAlignment(TextAlignment.CENTER);

        document.add(paragraph1);
        document.add(paragraph2);
        document.add(paragraph3);
        document.add(paragraph4);
        document.close();
    }

}

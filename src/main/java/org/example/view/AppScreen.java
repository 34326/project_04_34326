package org.example.view;


import java.io.IOException;
import java.net.URL;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.example.database.SQLite;
import org.example.app.App;
import org.example.model.Film;


/**
 * Class responsible for the form, the choice of the day and the movie
 */
public class AppScreen implements Initializable {
    public Button dayOne;
    public Button dayTwo;
    public Button dayThree;
    public Button dayFour;
    public Button dayFive;
    public Button daySix;
    public Button daySeven;
    public Button dayEight;
    public Button dayNine;
    public Button dayTen;
    public Button dayEleven;
    public Button dayTwelve;
    public Button dayThirteen;
    public Button dayFourteen;
    public static String date;
    public static String film;
    public static String userTicketName;
    public static String email;
    public String ticketFilm;
    @FXML
    public Label myLabelOne;
    @FXML
    public Label myLabelTwo;
    @FXML
    private TextField userName;
    @FXML
    private ChoiceBox<String> filmChoice;
    @FXML
    private TextField userEmail;
    static public String newid;
    static public Integer ids;
    static public String par;


    /**
     * Public method responsible for approving the form and going to the selection of seats
     * @throws IOException
     */
    @FXML
    public void saveTicket() throws IOException {
        date = myLabelOne.getText();
        film = myLabelTwo.getText();
        userTicketName = userName.getText();
        email = userEmail.getText();
        String id=filmChoice.getValue();
        String[] parts = id.split(" ");
        newid= parts[0];
        ids=Integer.valueOf(newid);
        App.setRoot("startScreen");
    }

    /**
     * Public method which gets the current date and sets the days two weeks ahead
     */
    public void setDays(){
        DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("E d MMM");
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        dayOne.setText(dtf1.format(zonedDateTime));
        dayTwo.setText(dtf1.format(zonedDateTime.plusDays(1)));
        dayThree.setText(dtf1.format(zonedDateTime.plusDays(2)));
        dayFour.setText(dtf1.format(zonedDateTime.plusDays(3)));
        dayFive.setText(dtf1.format(zonedDateTime.plusDays(4)));
        daySix.setText(dtf1.format(zonedDateTime.plusDays(5)));
        daySeven.setText(dtf1.format(zonedDateTime.plusDays(6)));
        dayEight.setText(dtf1.format(zonedDateTime.plusDays(7)));
        dayNine.setText(dtf1.format(zonedDateTime.plusDays(8)));
        dayTen.setText(dtf1.format(zonedDateTime.plusDays(9)));
        dayEleven.setText(dtf1.format(zonedDateTime.plusDays(10)));
        dayTwelve.setText(dtf1.format(zonedDateTime.plusDays(11)));
        dayThirteen.setText(dtf1.format(zonedDateTime.plusDays(12)));
        dayFourteen.setText(dtf1.format(zonedDateTime.plusDays(13)));
    }

    /**
     * Calling setDays
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setDays();
    }

    /**
     * Public method that gets the title of the movie
     * @param event
     */
    public void getFilms(ActionEvent event){
        String myFilm = filmChoice.getValue();
        myLabelTwo.setText(" " + myFilm);
        ticketFilm = filmChoice.getValue();
    }

    /**
     * Public method after selecting the day, enters the movie titles into choicebox, previously downloaded from the database
     * @param event
     */
    public void getDate(ActionEvent event){
        Button btn = (Button) event.getSource();
        btn.getId();
        String myDate = btn.getText();
        myLabelOne.setText(myDate);
        filmChoice.getItems().removeAll(filmChoice.getItems());

        String day = myDate;
        String[] parts = day.split(" ");
        SQLite b = new SQLite();
        List<Film> films = b.selectFilms(parts[0]);
        par=parts[0];
        films.clear();
        films = b.selectFilms(parts[0]);
        
        filmChoice.getItems().addAll(String.valueOf(films.get(0)));
        filmChoice.getItems().addAll(String.valueOf(films.get(1)));
        filmChoice.getItems().addAll(String.valueOf(films.get(2)));
        filmChoice.setOnAction(this::getFilms);
    }
}


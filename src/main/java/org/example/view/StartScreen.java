package org.example.view;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import org.example.app.MakeTicket;
import org.example.database.SQLite;
import org.example.app.SendEmail;
import org.example.app.App;
import org.example.model.Film;

/**
 * Class responsible for taking seats in cinema
 */
public class StartScreen implements Initializable {

    String plac;
    public Label za;
    public Button confirm;
    public Button one;
    public Button two;
    public Button three;
    public Button four;
    public Button five;
    public Button six;
    public Button seven;
    public Button eight;
    public Button nine;
    public Button ten;
    public Button eleven;
    public Button twelve;
    public Button thirteen;
    public Button fourteen;
    public Button fifteen;
    public Button sixteen;
    public Button seventeen;
    public Button eighteen;
    public Button nineteen;
    public Button twenty;
    public Button vip;
    public Button norm;
    public Integer l;

    /**
     * Function which is initializing when we loading StartScreen
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        confirm.setDisable(true);
        SQLite b = new SQLite();
        List<Film> films = b.selectFilms(AppScreen.par);

        for(int i=0; i<films.size(); i++)
        {
            if(films.get(i).getId()==Integer.valueOf(AppScreen.newid)){
                l=i;
            }
        }


        one.setStyle("-fx-background-color: #c2cb01d9; ");
        two.setStyle("-fx-background-color: #c2cb01d9; ");
        three.setStyle("-fx-background-color: #c2cb01d9; ");
        four.setStyle("-fx-background-color: #c2cb01d9; ");
        five.setStyle("-fx-background-color: #c2cb01d9; ");
        six.setStyle("-fx-background-color: #c2cb01d9; ");
        seven.setStyle("-fx-background-color: #c2cb01d9; ");
        eight.setStyle("-fx-background-color: #c2cb01d9; ");
        nine.setStyle("-fx-background-color: #c2cb01d9; ");
        ten.setStyle("-fx-background-color: #c2cb01d9; ");


        eleven.setStyle("-fx-background-color: #a908a0d9; ");
        twelve.setStyle("-fx-background-color: #a908a0d9; ");
        thirteen.setStyle("-fx-background-color: #a908a0d9; ");
        fourteen.setStyle("-fx-background-color: #a908a0d9; ");
        fifteen.setStyle("-fx-background-color: #a908a0d9; ");
        sixteen.setStyle("-fx-background-color: #a908a0d9; ");
        seventeen.setStyle("-fx-background-color: #a908a0d9; ");
        eighteen.setStyle("-fx-background-color: #a908a0d9; ");
        nineteen.setStyle("-fx-background-color: #a908a0d9; ");
        twenty.setStyle("-fx-background-color: #a908a0d9; ");

        vip.setStyle("-fx-background-color: #c2cb01d9; ");
        norm.setStyle("-fx-background-color: #a908a0d9; ");

        if(films.get(l).m1==true)
        {
            one.setDisable(true);
        }
        if(films.get(l).m2==true)
        {
            two.setDisable(true);
        }
        if(films.get(l).m3==true)
        {
            three.setDisable(true);
        }
        if(films.get(l).m4==true)
        {
            four.setDisable(true);
        }
        if(films.get(l).m5==true)
        {
            five.setDisable(true);
        }
        if(films.get(l).m6==true)
        {
            six.setDisable(true);
        }
        if(films.get(l).m7==true)
        {
            seven.setDisable(true);
        }
        if(films.get(l).m8==true)
        {
            eight.setDisable(true);
        }
        if(films.get(l).m9==true)
        {
            nine.setDisable(true);
        }
        if(films.get(l).m10==true)
        {
            ten.setDisable(true);
        }
        if(films.get(l).m11==true)
        {
            eleven.setDisable(true);
        }
        if(films.get(l).m12==true)
        {
            twelve.setDisable(true);
        }
        if(films.get(l).m13==true)
        {
            thirteen.setDisable(true);
        }
        if(films.get(l).m14==true)
        {
            fourteen.setDisable(true);
        }
        if(films.get(l).m15==true)
        {
            fifteen.setDisable(true);
        }
        if(films.get(l).m16==true)
        {
            sixteen.setDisable(true);
        }
        if(films.get(l).m17==true)
        {
            seventeen.setDisable(true);
        }
        if(films.get(l).m18==true)
        {
            eighteen.setDisable(true);
        }
        if(films.get(l).m19==true)
        {
            nineteen.setDisable(true);
        }
        if(films.get(l).m20==true)
        {
            twenty.setDisable(true);
        }

    }

    /**
     * Function on every seats when we click in cinema
     * @param event
     */
    @FXML
    public void place(ActionEvent event){
        confirm.setDisable(false);
        Button btn = (Button) event.getSource();
        btn.getId();
        plac = btn.getText();
        za.setText("Miejsce zostało wybrane");

    }

    /**
     * Function confirm every change and sending email in cinema
     * @throws IOException
     */
    public void confirm() throws IOException{


        SQLite b = new SQLite();
        b.insertFilm(Integer.valueOf(plac), AppScreen.ids);
        MakeTicket.makeTicket(AppScreen.film,AppScreen.date,AppScreen.userTicketName);
        SendEmail.sendEmail(AppScreen.email);



        long start = System.currentTimeMillis();
        long end = start + 3 * 1000;
        while (System.currentTimeMillis() < end) {
            App.setRoot("appScreen");
        }


    }

}


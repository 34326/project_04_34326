module org.example {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires kernel;
    requires layout;
    requires java.mail;
    requires activation;
    requires java.sql;
    requires barcodes;


    exports org.example.app;
    opens org.example.app;
    exports org.example.model;
    opens org.example.model;
    exports org.example.view;
    opens org.example.view;
    exports org.example.database;
    opens org.example.database;
}
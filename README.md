# Cinema #

## Table of contents
* [General info](#markdown-header-general-info)
* [Technologies](#markdown-header-technologies)
* [Instruction](#markdown-header-instruction)
* [Distribution of tasks](#markdown-header-distribution-of-tasks)
* [Gallery](#markdown-header-gallery)
* [Created by](#markdown-header-created-by)

## General info
This project was created for school project.
	
## Technologies
* Java- SDK version 17
* iText
* SQLite
* JavaMail
* Maven
	
## Instruction
To run this project:
```
1.Run project.
2.Enter data into the form.
3.Choose day and film.
4.Press "Zatwierdź".
5.Choose a seat.
6.Press "Zamów" 
```

### Distribution of tasks ###

| Daniel Sikora | Grzegorz Tokarz            |
|---------------|----------------------------|
| Gui           | Database       		          |
| Sending email | Functions using a database | 
| Documentation | Documentation              |

### Gallery ###
![Form](https://cdn.discordapp.com/attachments/829627940648124426/968153177776095252/unknown.png)
![Seat chooser](https://cdn.discordapp.com/attachments/829627940648124426/968153285892665344/unknown.png)

### Created by: ###
* Grzegorz Tokarz [BITBUCKET](https://bitbucket.org/grzecho123)
* Daniel Sikora   [BITBUCKET](https://bitbucket.org/34326)